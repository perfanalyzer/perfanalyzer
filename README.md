# PerfAnalyzer

This tool is designed for generating graphs based on performance counters (--pex) in Veeam logs.
It helps detecting the bootlenecks during backup/restore.

Feaures:
- several log files can be added
- per proxy selection
- summary graph with adjustible smoothness
- command line support

Supported Veeam log files:
- VM backup (Hyper-V and VMWare)
- Backup To Tape (including virtual full)
- File Copy (without displaying file names)
- Entire VM restore
- Restore from Tape

How to use: drag and drop the corresponding log file (either Job or Task log file) containing "--pex" entries.

![image.png](./image.png)

