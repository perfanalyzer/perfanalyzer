﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PerfAnalyzer.Presentation
{
    public interface ILogger
    {
        void Log(string message);
    }
    public class RichTextBoxLogger : ILogger
    {
        private readonly RichTextBox _richTextBox;
        public RichTextBoxLogger(RichTextBox richTextBox)
        {
            _richTextBox = richTextBox;
        }

        //private delegate void SafeCallDelegate(string message);
        //public void Log(string message)
        //{
        //    if (_richTextBox.InvokeRequired)
        //    {
        //        var d = new SafeCallDelegate(LogSafeCall);
        //        _richTextBox.Invoke(d, new object[] { message });

        //    }
        //    else
        //    {
        //        LogSafeCall(message);
        //    }
        //}

        public void Log(string message)
        {
            _richTextBox.AppendText(message);
            _richTextBox.AppendText("\n");
            //_richTextBox.SelectionStart = _richTextBox.TextLength;
            //_richTextBox.ScrollToCaret();
        }

    }
}
