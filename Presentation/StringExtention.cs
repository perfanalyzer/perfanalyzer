﻿using PerfAnalyzer.Domain;
using PerfAnalyzer.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfAnalyzer.Presentation
{    
    internal static class StringExtention
    {
        private static readonly string[] _TASKRUN_BEGIN = { "Starting new log"};
        private static readonly string[] _TASKRUN_BEGIN_ENTIRE_VM_RESTORE = { "===================================================================" };
        
        private static readonly string[] _START_AGENT_VMWARE_BACKUP = { "Invoke: DataTransfer.BackupText", ".vmdk@" };
        private static readonly string[] _START_AGENT_HV_BACKUP = { "Invoke: DataTransfer.SyncDisk", ".vhdx" };
        private static readonly string[] _START_AGENT_BACKUP_TO_TAPE_VBK = { "backupSrvFileToTape", ".vbk" };
        private static readonly string[] _START_AGENT_BACKUP_TO_TAPE_VIB = { "backupSrvFileToTape", ".vib" };
        private static readonly string[] _START_AGENT_BACKUP_TO_TAPE_VSB = { "backupSrvSynthesizedStgToTape", ".vsb" };  //TBD
        private static readonly string[] _START_AGENT_FILE_COPY_JOB = { "getWithPermissions" };
        //private static readonly string[] _BACKUP_FILES_EXTENTIONS = { ".vbk", ".vib" };
        private static readonly string[] _START_AGENT_ENTIRE_VM_RESTORE_VMWARE = { "Invoke: DataTransfer.SyncDisk", "Source.Type = {backup}", "flat.vmdk@" };
        private static readonly string[] _START_AGENT_ENTIRE_VM_RESTORE_FROM_TAPE = { "executeTapeScript", "restoreSrvFileFromTape"};

        private static readonly string[] _VM_BACKUP_TASKRUN_END_TIMESTAMP = { "Task session", "has been completed" };
        private static readonly string[] _TAPE_BACKUP_TASKRUN_END_TIMESTAMP = { "task", "has been completed" };
        private static readonly string[] _TAPE_RESTORE_TASKRUN_END_TIMESTAMP = { " Script finished" };
        private static readonly string[] _STOPPED_BY_USER_TIMESTAMP = { "Operation was canceled by user" };
              

        const string _TASKRUN_BEGIN_VM_BACKUP_TIMESTAMP = "Set status 'InProgress' for task session";
        const string _TASKRUN_BEGIN_TAPE_BACKUP_TIMESTAMP = "Creating task for";
        const string _TASKRUN_BEGIN_ENTIRE_VM_RESTORE_FROM_TAPE_TIMESTAMP = " STARTTAPEVMRESTORE";

        const string _AGENT_MARKER = "[AP] (";
        const string _VMWARE_DISK_EXTENTION = ".vmdk@";
        const string _HV_DISK_EXTENTION = ".vhdx";
        const string _VBK = ".vbk";
        const string _VIB = ".vib";
        const string _AGENT_START = "Starting client agent session, id";
        const string _PEX = "--pex";

        //As Entire VM restore doesn't contain "Starting new log" but always starts with "==================================================================="
        //as workaround the first line is replaced to "Starting new log" on the fly
        public static string FixForEntireVMRestore(this string str)
        {
            if (str.ContainsAll(_TASKRUN_BEGIN_ENTIRE_VM_RESTORE))
            {
                return string.Join(" ", _TASKRUN_BEGIN);
            }
            return str;
        }

        private static bool ContainsAll(this string str, string[] substrings)
        {
            int matches = 0;

            foreach (var s in substrings)
            {
                if (str.Contains(s))
                {
                    matches++;
                }
                else
                {
                    break;
                }
            }

            if (matches == substrings.Count())
            { 
                return true; 
            }
            else 
            { 
                return false; 
            }
        }

        public static string ParseTimeStamp(this string str)
        {
            return str.Substring(1, 19); //returns string like "28.12.2020 14:25:21"
        }

        public static bool IsTaskRunBegin(this string str)
        {
            //return str.Contains("Starting new log");
            return str.ContainsAll(_TASKRUN_BEGIN);
        }

        //public static bool IsAgentBegin(this string str)
        //{
        //    if ((str.Contains("Invoke: DataTransfer.BackupText") && str.Contains(".vmdk@"))                 //VMware backup
        //        || (str.Contains("Invoke: DataTransfer.SyncDisk") && str.Contains(".vhdx"))                 //HV backup
        //        || (str.Contains("backupSrvFileToTape") && (str.Contains(".vbk") || str.Contains(".vib")))  //Backup to Tape

        //    //TBD. DO NOT DELETE
        //    //|| (str.Contains("backupSrvSynthesizedStgToTape") && (str.Contains(".vsb")))                //Backup to Tape, virtual full
        //    //|| (str.Contains("Invoke: DataTransfer.SyncDisk") && (str.Contains(".vmdk@")))              //Entire VM restore to VMWare
        //    //|| (str.Contains("getWithPermissions") && (str.Contains(".vbk") || str.Contains(".vmdk")))   //TEST file copy to ESXi host
        //    //|| (str.Contains("executeTapeScript") && (str.Contains(".vbk")))                             //TEST tape restore   
        //    )
        public static bool IsAgentBegin(this string str)
        {
            if (str.ContainsAll(_START_AGENT_VMWARE_BACKUP)
                || str.ContainsAll(_START_AGENT_HV_BACKUP)
                || str.ContainsAll(_START_AGENT_BACKUP_TO_TAPE_VBK)
                || str.ContainsAll(_START_AGENT_BACKUP_TO_TAPE_VIB)
                || str.ContainsAll(_START_AGENT_BACKUP_TO_TAPE_VSB)  //TBD
                || str.ContainsAll(_START_AGENT_ENTIRE_VM_RESTORE_VMWARE)
                || str.ContainsAll(_START_AGENT_FILE_COPY_JOB)
                || str.ContainsAll(_START_AGENT_ENTIRE_VM_RESTORE_FROM_TAPE)
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsTaskRunBeginTimestamp(this string str)
        {
            //if (str.Contains("Set status 'InProgress' for task session") || str.Contains("Creating task for"))
            if (str.Contains(_TASKRUN_BEGIN_VM_BACKUP_TIMESTAMP)
                || str.Contains(_TASKRUN_BEGIN_TAPE_BACKUP_TIMESTAMP)
                || str.Contains(_TASKRUN_BEGIN_ENTIRE_VM_RESTORE_FROM_TAPE_TIMESTAMP)
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static string GetAgentName(this string str)
        {
            //int i = str.IndexOf("[AP] (");
            int i = str.IndexOf(_AGENT_MARKER);
            return str.Substring(i + 6, 4); //agent name - 4 digits   
        }

        public static string GetDiskName(this string str)
        {
            string diskname = string.Empty;
            //if (str.Contains(".vmdk@"))
            if (str.Contains(_VMWARE_DISK_EXTENTION))
            {
                diskname = str.Substring(0, str.IndexOf(_VMWARE_DISK_EXTENTION) + 5); //cut the end of the string
                diskname = diskname.Substring(diskname.LastIndexOf("/") + 1);
            }
            else 
            //if (str.Contains(".vhdx"))
            if (str.Contains(_HV_DISK_EXTENTION))
            {
                diskname = str.Substring(0, str.IndexOf(_HV_DISK_EXTENTION) + 5); //cut the end of the string
                diskname = diskname.Substring(diskname.LastIndexOf("\\") + 1);
            }
            else
            //if (  str.Contains(".vbk") || str.Contains(".vib"))
            if (str.Contains(_VBK))
            {
                diskname = str.Substring(0, str.IndexOf(_VBK) + 4); 
                diskname = diskname.Substring(diskname.LastIndexOf("\\") + 1);
                diskname = diskname.Substring(diskname.LastIndexOf("/") + 1);
            }    
            else if (str.Contains(_VIB))
            {
                diskname = str.Substring(0, str.IndexOf(_VIB) + 4);
                diskname = diskname.Substring(diskname.LastIndexOf("\\") + 1);
                diskname = diskname.Substring(diskname.LastIndexOf("/") + 1);
            }
            return diskname;
        }

        //[28.12.2020 15:16:09] <56> Info                   [AP] (f367) output: --pex:54;603891695616;232188280832;371506282496;232188280832;232190716840;4;46;99;10;46;99;132536349690850000
        public static Pex GetPexData(this string str)
        {
            Pex pex = new Pex();

            try
            {
                DateTime dateTime = DateTime.Parse(ParseTimeStamp(str));
                string[] s = str.Split(new char[] { ';' });
                string aname = GetAgentName(str);

                pex.TimeStamp = dateTime;
                pex.AgentName = aname;

                s[0] = s[0].Substring(s[0].LastIndexOf(":") + 1);
                pex.Progress = Convert.ToInt32(s[0]);

                pex.DataRead = Convert.ToInt64(s[2]);
                pex.DataTransferred = Convert.ToInt64(s[5]);
                pex.Pex1 = Convert.ToInt32(s[6]);
                pex.Pex2 = Convert.ToInt32(s[7]);
                pex.Pex3 = Convert.ToInt32(s[8]);
                pex.Pex4 = Convert.ToInt32(s[9]);
                pex.Pex5 = Convert.ToInt32(s[10]);
                pex.Pex6 = Convert.ToInt32(s[11]);
            }
            catch (Exception ex)
            {
                throw new MyCustomException($"Invalid Pex data, ErrorDetails: {ex.Message}");
            }
            return pex;
        }

        //[18.01.2021 19:58:48] <25> Info         [ProxyAgent] Starting client agent session, id '841ac023-6fbb-4685-8861-b642c7d8897d', host 'mmez-w2016', agent id 'e535b90a-0a7f-4984-8c84-32d073c93968', IPs '172.17.13.195:2500', PID '7616'
        public static string GetProxyName(this string aname, List<string> agentstarts)
        {
            string proxyname = string.Empty;
            foreach (string proxy in agentstarts)
            {
                if (proxy.Contains($"'{aname}"))
                {
                    proxyname = proxy.Substring(0, proxy.IndexOf("', agent id "));
                    proxyname = proxyname.Substring(proxy.IndexOf(", host '") + 8);
                }
            }
            return proxyname;
        }

        //[18.01.2021 19:58:48] <25> Info         [ProxyAgent] Starting client agent session, id '841ac023-6fbb-4685-8861-b642c7d8897d', host 'mmez-w2016', agent id 'e535b90a-0a7f-4984-8c84-32d073c93968', IPs '172.17.13.195:2500', PID '7616'
        public static bool IsAnyAgentStart(this string str)
        {
            if (str.Contains(_AGENT_START))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsPex(this string str)
        {
            if (str.Contains(_PEX))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsTaskRunEnd(this string str)
        {
            //if (
            //    (str.Contains("Task session") || str.Contains("task"))
            //    && str.Contains("has been completed")
            //    )
            if(str.ContainsAll(_VM_BACKUP_TASKRUN_END_TIMESTAMP)
                || str.ContainsAll(_TAPE_BACKUP_TASKRUN_END_TIMESTAMP)
                || str.ContainsAll(_TAPE_RESTORE_TASKRUN_END_TIMESTAMP)
                || str.ContainsAll(_STOPPED_BY_USER_TIMESTAMP)
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
