﻿using PerfAnalyzer.Domain;
using PerfAnalyzer.Exceptions;
using PerfAnalyzer.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PerfAnalyzer.Presentation
{
    public class Presenter
    {
        private readonly MainForm _view;
        private readonly ILogger _logger;
        private readonly List<TaskFile> _taskFiles;
        private readonly List<Proxy> _proxies;
        public Presenter(string[] args)
        {
            _view = new MainForm(this,args);
            _logger = new RichTextBoxLogger(_view.richTextBoxLog);
            _view.logger = _logger;

            _taskFiles = new List<TaskFile>();
            _proxies = new List<Proxy>();
        }
        public void Run()
        {          
            _view.Show();
        }

        public void RefreshGraph()
        {
            _view.Draw(_taskFiles);
        }

        void ProcessLogFile(string filename)
        {
            TaskFile taskFile = null;
            List<string> taskRunSessionsNames;
            try
            {
                taskFile = ParseFile(filename);
            }
            catch (MyCustomException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            if (taskFile != null)
            {
                taskRunSessionsNames = GetTaskRunSessionsNames(taskFile);
                _view.GenerateTaskComboBox(filename, taskRunSessionsNames);
                _logger.Log($"Number of sessions found: {taskFile.TaskRuns.Count}\n");
                _taskFiles.Add(taskFile);
            }
        }

        List<string> GetTaskRunSessionsNames(TaskFile taskFile)
        {
            var results = new List<string>();

            foreach (var taskRun in taskFile.TaskRuns)
            {
                if (taskRun.TaskRunEndTime == DateTime.MinValue)
                {
                    results.Add((taskRun.TaskRunStartTime).ToString() + " - STILL RUNNING");
                }
                else
                {
                    results.Add((taskRun.TaskRunStartTime).ToString() + " - " + (taskRun.TaskRunEndTime).ToString());
                }
            }

            return results;
        }


        TaskFile ParseFile(string filename)
        {
            var taskRunCounter = 0;
            bool taskStarted = false;
            var taskFile = new TaskFile();
            var line_number = 0;
            string str = string.Empty;

            _logger.Log($"Parsing file {filename}");

            using (StreamReader filestream = new StreamReader(filename))
            {
                try
                {
                    while ((str = filestream.ReadLine()) != null)
                    {
                        line_number++;

                        if (line_number == 1)
                            str = str.FixForEntireVMRestore();

                        //start of new task session
                        if (str.IsTaskRunBegin())
                        {
                            taskStarted = true;
                            taskFile.TaskRuns.Add(new TaskRun());
                            _logger.Log($"Task session: {taskRunCounter + 1}");
                        }

                        //Fill TaskRun item with data
                        if (taskStarted)
                        {
                            taskFile.TaskRuns[taskRunCounter] = FillTaskRunWithData(str, taskFile.TaskRuns[taskRunCounter]);
                            //end of task session
                            if (str.IsTaskRunEnd())
                            {
                                taskStarted = false;
                                taskFile.TaskRuns[taskRunCounter].TaskRunEndTime = DateTime.Parse(str.ParseTimeStamp());
                                taskRunCounter++;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new MyCustomException($"File: {filename}\n\nParse error in line {line_number}, ErrorDetails: {ex.Message}");
                }
            }
            return taskFile;
        }

        TaskRun FillTaskRunWithData(string str, TaskRun taskRun)
        {
            //timestamp of TaskRun start
            if (str.IsTaskRunBeginTimestamp())
            {
                taskRun.TaskRunStartTime = DateTime.Parse(str.ParseTimeStamp());
            }
            //Adding agents
            else if (str.IsAgentBegin())
            {
                AddAgentIntoTaskRun(str, taskRun);
                AddProxyIfNeeded(str, taskRun);            
                _logger.Log($"Adding agent: {str.GetAgentName()}");
            }
            //determining the string with the name of the proxy
            //[18.01.2021 19:59:05] <36> Info         [ProxyAgent] Starting client agent session, id 'd751acbe-1416-4d13-adce-6953a460f8e9', host 'mmez-w2016', agent id 'e535b90a-0a7f-4984-8c84-32d073c93968', IPs '172.17.13.195:2500', PID '7616'
            else if (str.IsAnyAgentStart())
            {
                taskRun.AgentStarts.Add(str);
            }
            //Adding pexes
            else if (str.IsPex())
            {
                AddPexIntoAgent(str, ref taskRun.Agents);               
            }
            return taskRun;
        }


        void AddAgentIntoTaskRun(string str, TaskRun taskRun)
        {
            string aname = str.GetAgentName();
            string diskname = str.GetDiskName();
            string proxyname = aname.GetProxyName(taskRun.AgentStarts);
            taskRun.Agents.Add(new Agent(aname, diskname, proxyname));
        }

        void AddProxyIfNeeded(string str, TaskRun taskRun)
        {
            string aname = str.GetAgentName();
            string diskname = str.GetDiskName();
            string proxyname = aname.GetProxyName(taskRun.AgentStarts);

            if (IsProxyAlreadyAdded(proxyname) == false)
            {
                _proxies.Add(new Proxy(proxyname));
                _view.AddProxyToComboBox(proxyname);
            }
        }

        void AddPexIntoAgent(string str, ref List<Agent> agents)
        {
            string aname = str.GetAgentName();
            foreach (var agent in agents)
            {
                if (agent.AgentName == aname)
                {
                    Pex pex = str.GetPexData();

                    int pexcount = agent.Pexes.Count;
                    if (pexcount == 0)
                    {
                        pex.DataReadSpeed = 0;
                        pex.DataTransferredSpeed = 0;
                    }
                    else
                    {
                        pex.DataReadSpeed = pex.CalculateDataReadSpeed(pex, agent.Pexes[pexcount - 1]);
                        pex.DataTransferredSpeed = pex.CalculateDataTransferredSpeed(pex, agent.Pexes[pexcount - 1]);
                    }
                    agent.Pexes.Add(pex);
                    break;
                }
            }
        }

        bool IsProxyAlreadyAdded(string proxyname)
        {
            bool proxyAlreadyAdded = false;
            foreach (var proxy in _proxies)
            {
                if (proxy.Name == proxyname)
                {
                    proxyAlreadyAdded = true;
                    break;
                }
            }
            return proxyAlreadyAdded;
        }       

        public void ClearAll()
        {
            _taskFiles.Clear();
            _proxies.Clear();
            _view.ClearTaskComboBoxes();
            _view.HideGraphControls();
            _logger.Log("Clear all button pressed.\n");
            //RefreshGraph();
        }

        public void HandleSingleFile(string filename)
        {
            ProcessLogFile(filename);           
            RefreshGraph();
        }

        public void HandleSeveralFiles(string[] files)
        {
            foreach (string f in files)
            {
                ProcessLogFile(f);
            }
            RefreshGraph();
        }

    }
}
