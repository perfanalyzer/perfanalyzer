﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PerfAnalyzer.Domain
{
    class TaskComboBox
    {
        public const int TOP = 5;
        public static int TaskCounter = 0;
        public int SelectedTaskRun = 0;
        public static int left = 10;
        public static int top = TOP;
        int width = 230;
        int offset = 50;

        public ComboBox comboBoxTask = new ComboBox();
        public Label labelTask = new Label();

 //       public int TaskRunCounter { get; set; }
        public TaskComboBox()
        {
            TaskCounter++;

            labelTask.Left = left;
            labelTask.Top = top;
            labelTask.Width = width;

            comboBoxTask.Left = left;
            comboBoxTask.Top = top + 17;
            comboBoxTask.Width = width;

            top += offset;
        }

        public int getSelectedIndex()
        {
            return comboBoxTask.SelectedIndex;
        }
    }
}
