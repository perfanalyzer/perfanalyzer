﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfAnalyzer.Presentation
{
    public enum GraphType
    {
        Read,
        Transferred,
        Progress,
        PexSource,
        PexProxy,
        PexNetworkS,
        PexNetworkT,
        PexTarget
    }
}
