﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PerfAnalyzer.Settings;

namespace PerfAnalyzer
{
    public partial class FormAbout : Form
    {
        //const string VERSION = "0.1.2b";
        string version = VersionNumber.GetInformationalVersion(Assembly.GetExecutingAssembly());
        public FormAbout()
        {
            InitializeComponent();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormAbout_Shown(object sender, EventArgs e)
        {
            richTextBoxAbout.AppendText($"Version: {version}\n\n");
            richTextBoxAbout.AppendText($"Supported log files:\n" +
                $"- VM Backup (Hyper-V, VMWare)\n" +
                $"- Backup To Tape (including virtual full)\n" +
                $"- Entire VM Restore\n" +
                $"- Restore From Tape\n" +
                $"- File Copy (partially)\n" +
                $"\n");

            richTextBoxAbout.AppendText("If you have some suggestions regarding improvements, please feel free to contact michail.mezrin@veeam.com");
        }
    }
}
