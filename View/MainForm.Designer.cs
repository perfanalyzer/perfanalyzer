﻿
namespace PerfAnalyzer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.radioButtonRead = new System.Windows.Forms.RadioButton();
            this.radioButtonTransferred = new System.Windows.Forms.RadioButton();
            this.Left = new System.Windows.Forms.Button();
            this.Right = new System.Windows.Forms.Button();
            this.Plus = new System.Windows.Forms.Button();
            this.Minus = new System.Windows.Forms.Button();
            this.radioButtonLine = new System.Windows.Forms.RadioButton();
            this.radioButtonPoint = new System.Windows.Forms.RadioButton();
            this.radioButtonColumn = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonProgress = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonPexNetworkT = new System.Windows.Forms.RadioButton();
            this.radioButtonPexTarget = new System.Windows.Forms.RadioButton();
            this.radioButtonPexNetworkS = new System.Windows.Forms.RadioButton();
            this.radioButtonPexProxy = new System.Windows.Forms.RadioButton();
            this.radioButtonPexSource = new System.Windows.Forms.RadioButton();
            this.textBoxMaxY = new System.Windows.Forms.TextBox();
            this.checkBoxAutoY = new System.Windows.Forms.CheckBox();
            this.labelY = new System.Windows.Forms.Label();
            this.groupBoxTaskSessions = new System.Windows.Forms.GroupBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.checkBoxSum = new System.Windows.Forms.CheckBox();
            this.comboBoxProxy = new System.Windows.Forms.ComboBox();
            this.labelProxy = new System.Windows.Forms.Label();
            this.textBoxSum = new System.Windows.Forms.TextBox();
            this.panelTaskSessions = new System.Windows.Forms.Panel();
            this.labelDragAndDropMessage = new System.Windows.Forms.Label();
            this.labelSum = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1484, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // chart1
            // 
            this.chart1.AllowDrop = true;
            chartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            chartArea1.AxisX.IsLabelAutoFit = false;
            chartArea1.AxisX.LabelAutoFitMaxFontSize = 8;
            chartArea1.AxisX.LabelStyle.Angle = -90;
            chartArea1.AxisX.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Minutes;
            chartArea1.AxisX.MinorTickMark.Enabled = true;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(289, 29);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Color = System.Drawing.Color.Red;
            series1.Legend = "Legend1";
            series1.Name = "Agent";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Time;
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(1183, 471);
            this.chart1.TabIndex = 1;
            this.chart1.Text = "chart1";
            this.chart1.DragDrop += new System.Windows.Forms.DragEventHandler(this.chart1_DragDrop);
            this.chart1.DragEnter += new System.Windows.Forms.DragEventHandler(this.chart1_DragEnter);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 579);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Log";
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Location = new System.Drawing.Point(12, 598);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.ReadOnly = true;
            this.richTextBoxLog.Size = new System.Drawing.Size(1460, 127);
            this.richTextBoxLog.TabIndex = 4;
            this.richTextBoxLog.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "log";
            // 
            // radioButtonRead
            // 
            this.radioButtonRead.AutoSize = true;
            this.radioButtonRead.Checked = true;
            this.radioButtonRead.Location = new System.Drawing.Point(6, 19);
            this.radioButtonRead.Name = "radioButtonRead";
            this.radioButtonRead.Size = new System.Drawing.Size(83, 17);
            this.radioButtonRead.TabIndex = 4;
            this.radioButtonRead.TabStop = true;
            this.radioButtonRead.Text = "Read, MB/s";
            this.radioButtonRead.UseVisualStyleBackColor = true;
            this.radioButtonRead.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // radioButtonTransferred
            // 
            this.radioButtonTransferred.AutoSize = true;
            this.radioButtonTransferred.Location = new System.Drawing.Point(138, 19);
            this.radioButtonTransferred.Name = "radioButtonTransferred";
            this.radioButtonTransferred.Size = new System.Drawing.Size(111, 17);
            this.radioButtonTransferred.TabIndex = 5;
            this.radioButtonTransferred.Text = "Transferred, MB/s";
            this.radioButtonTransferred.UseVisualStyleBackColor = true;
            this.radioButtonTransferred.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // Left
            // 
            this.Left.Location = new System.Drawing.Point(306, 516);
            this.Left.Name = "Left";
            this.Left.Size = new System.Drawing.Size(75, 23);
            this.Left.TabIndex = 6;
            this.Left.Text = "<=";
            this.Left.UseVisualStyleBackColor = true;
            this.Left.Click += new System.EventHandler(this.Left_Click);
            // 
            // Right
            // 
            this.Right.Location = new System.Drawing.Point(404, 516);
            this.Right.Name = "Right";
            this.Right.Size = new System.Drawing.Size(73, 23);
            this.Right.TabIndex = 7;
            this.Right.Text = "=>";
            this.Right.UseVisualStyleBackColor = true;
            this.Right.Click += new System.EventHandler(this.Right_Click);
            // 
            // Plus
            // 
            this.Plus.Location = new System.Drawing.Point(554, 516);
            this.Plus.Name = "Plus";
            this.Plus.Size = new System.Drawing.Size(26, 26);
            this.Plus.TabIndex = 8;
            this.Plus.Text = "+";
            this.Plus.UseVisualStyleBackColor = true;
            this.Plus.Click += new System.EventHandler(this.Plus_Click);
            // 
            // Minus
            // 
            this.Minus.Location = new System.Drawing.Point(519, 516);
            this.Minus.Name = "Minus";
            this.Minus.Size = new System.Drawing.Size(29, 26);
            this.Minus.TabIndex = 9;
            this.Minus.Text = "-";
            this.Minus.UseVisualStyleBackColor = true;
            this.Minus.Click += new System.EventHandler(this.Minus_Click);
            // 
            // radioButtonLine
            // 
            this.radioButtonLine.AutoSize = true;
            this.radioButtonLine.Checked = true;
            this.radioButtonLine.Location = new System.Drawing.Point(6, 22);
            this.radioButtonLine.Name = "radioButtonLine";
            this.radioButtonLine.Size = new System.Drawing.Size(45, 17);
            this.radioButtonLine.TabIndex = 10;
            this.radioButtonLine.TabStop = true;
            this.radioButtonLine.Text = "Line";
            this.radioButtonLine.UseVisualStyleBackColor = true;
            this.radioButtonLine.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // radioButtonPoint
            // 
            this.radioButtonPoint.AutoSize = true;
            this.radioButtonPoint.Location = new System.Drawing.Point(77, 22);
            this.radioButtonPoint.Name = "radioButtonPoint";
            this.radioButtonPoint.Size = new System.Drawing.Size(49, 17);
            this.radioButtonPoint.TabIndex = 11;
            this.radioButtonPoint.Text = "Point";
            this.radioButtonPoint.UseVisualStyleBackColor = true;
            this.radioButtonPoint.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // radioButtonColumn
            // 
            this.radioButtonColumn.AutoSize = true;
            this.radioButtonColumn.Location = new System.Drawing.Point(142, 22);
            this.radioButtonColumn.Name = "radioButtonColumn";
            this.radioButtonColumn.Size = new System.Drawing.Size(47, 17);
            this.radioButtonColumn.TabIndex = 12;
            this.radioButtonColumn.Text = "Area";
            this.radioButtonColumn.UseVisualStyleBackColor = true;
            this.radioButtonColumn.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonColumn);
            this.groupBox2.Controls.Add(this.radioButtonLine);
            this.groupBox2.Controls.Add(this.radioButtonPoint);
            this.groupBox2.Location = new System.Drawing.Point(609, 506);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(232, 53);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Graph style";
            // 
            // radioButtonProgress
            // 
            this.radioButtonProgress.AutoSize = true;
            this.radioButtonProgress.Location = new System.Drawing.Point(260, 19);
            this.radioButtonProgress.Name = "radioButtonProgress";
            this.radioButtonProgress.Size = new System.Drawing.Size(80, 17);
            this.radioButtonProgress.TabIndex = 14;
            this.radioButtonProgress.TabStop = true;
            this.radioButtonProgress.Text = "Progress, %";
            this.radioButtonProgress.UseVisualStyleBackColor = true;
            this.radioButtonProgress.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonPexNetworkT);
            this.groupBox1.Controls.Add(this.radioButtonPexTarget);
            this.groupBox1.Controls.Add(this.radioButtonPexNetworkS);
            this.groupBox1.Controls.Add(this.radioButtonPexProxy);
            this.groupBox1.Controls.Add(this.radioButtonPexSource);
            this.groupBox1.Controls.Add(this.radioButtonRead);
            this.groupBox1.Controls.Add(this.radioButtonProgress);
            this.groupBox1.Controls.Add(this.radioButtonTransferred);
            this.groupBox1.Location = new System.Drawing.Point(847, 506);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(418, 86);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data to display";
            // 
            // radioButtonPexNetworkT
            // 
            this.radioButtonPexNetworkT.AutoSize = true;
            this.radioButtonPexNetworkT.Location = new System.Drawing.Point(6, 65);
            this.radioButtonPexNetworkT.Name = "radioButtonPexNetworkT";
            this.radioButtonPexNetworkT.Size = new System.Drawing.Size(108, 17);
            this.radioButtonPexNetworkT.TabIndex = 19;
            this.radioButtonPexNetworkT.TabStop = true;
            this.radioButtonPexNetworkT.Text = "Pex network T, %";
            this.radioButtonPexNetworkT.UseVisualStyleBackColor = true;
            this.radioButtonPexNetworkT.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // radioButtonPexTarget
            // 
            this.radioButtonPexTarget.AutoSize = true;
            this.radioButtonPexTarget.Location = new System.Drawing.Point(260, 65);
            this.radioButtonPexTarget.Name = "radioButtonPexTarget";
            this.radioButtonPexTarget.Size = new System.Drawing.Size(87, 17);
            this.radioButtonPexTarget.TabIndex = 18;
            this.radioButtonPexTarget.TabStop = true;
            this.radioButtonPexTarget.Text = "Pex target, %";
            this.radioButtonPexTarget.UseVisualStyleBackColor = true;
            this.radioButtonPexTarget.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // radioButtonPexNetworkS
            // 
            this.radioButtonPexNetworkS.AutoSize = true;
            this.radioButtonPexNetworkS.Location = new System.Drawing.Point(260, 42);
            this.radioButtonPexNetworkS.Name = "radioButtonPexNetworkS";
            this.radioButtonPexNetworkS.Size = new System.Drawing.Size(108, 17);
            this.radioButtonPexNetworkS.TabIndex = 17;
            this.radioButtonPexNetworkS.TabStop = true;
            this.radioButtonPexNetworkS.Text = "Pex network S, %";
            this.radioButtonPexNetworkS.UseVisualStyleBackColor = true;
            this.radioButtonPexNetworkS.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // radioButtonPexProxy
            // 
            this.radioButtonPexProxy.AutoSize = true;
            this.radioButtonPexProxy.Location = new System.Drawing.Point(138, 42);
            this.radioButtonPexProxy.Name = "radioButtonPexProxy";
            this.radioButtonPexProxy.Size = new System.Drawing.Size(85, 17);
            this.radioButtonPexProxy.TabIndex = 16;
            this.radioButtonPexProxy.TabStop = true;
            this.radioButtonPexProxy.Text = "Pex proxy, %";
            this.radioButtonPexProxy.UseVisualStyleBackColor = true;
            this.radioButtonPexProxy.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // radioButtonPexSource
            // 
            this.radioButtonPexSource.AutoSize = true;
            this.radioButtonPexSource.Location = new System.Drawing.Point(6, 42);
            this.radioButtonPexSource.Name = "radioButtonPexSource";
            this.radioButtonPexSource.Size = new System.Drawing.Size(92, 17);
            this.radioButtonPexSource.TabIndex = 15;
            this.radioButtonPexSource.TabStop = true;
            this.radioButtonPexSource.Text = "Pex source, %";
            this.radioButtonPexSource.UseVisualStyleBackColor = true;
            this.radioButtonPexSource.CheckedChanged += new System.EventHandler(this.Refresh_Display);
            // 
            // textBoxMaxY
            // 
            this.textBoxMaxY.Enabled = false;
            this.textBoxMaxY.Location = new System.Drawing.Point(290, 68);
            this.textBoxMaxY.Name = "textBoxMaxY";
            this.textBoxMaxY.Size = new System.Drawing.Size(48, 20);
            this.textBoxMaxY.TabIndex = 16;
            this.textBoxMaxY.Visible = false;
            this.textBoxMaxY.TextChanged += new System.EventHandler(this.textBoxMaxY_TextChanged);
            // 
            // checkBoxAutoY
            // 
            this.checkBoxAutoY.AutoSize = true;
            this.checkBoxAutoY.Checked = true;
            this.checkBoxAutoY.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxAutoY.Location = new System.Drawing.Point(290, 45);
            this.checkBoxAutoY.Name = "checkBoxAutoY";
            this.checkBoxAutoY.Size = new System.Drawing.Size(48, 17);
            this.checkBoxAutoY.TabIndex = 17;
            this.checkBoxAutoY.Text = "Auto";
            this.checkBoxAutoY.UseVisualStyleBackColor = true;
            this.checkBoxAutoY.Visible = false;
            this.checkBoxAutoY.CheckedChanged += new System.EventHandler(this.checkBoxAutoY_CheckedChanged);
            // 
            // labelY
            // 
            this.labelY.AutoSize = true;
            this.labelY.Location = new System.Drawing.Point(286, 29);
            this.labelY.Name = "labelY";
            this.labelY.Size = new System.Drawing.Size(34, 13);
            this.labelY.TabIndex = 18;
            this.labelY.Text = "MaxY";
            this.labelY.Visible = false;
            // 
            // groupBoxTaskSessions
            // 
            this.groupBoxTaskSessions.Location = new System.Drawing.Point(10, 27);
            this.groupBoxTaskSessions.Name = "groupBoxTaskSessions";
            this.groupBoxTaskSessions.Size = new System.Drawing.Size(273, 524);
            this.groupBoxTaskSessions.TabIndex = 19;
            this.groupBoxTaskSessions.TabStop = false;
            this.groupBoxTaskSessions.Text = "Task sessions";
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(1271, 559);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(91, 32);
            this.buttonClear.TabIndex = 20;
            this.buttonClear.Text = "Clear all";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // checkBoxSum
            // 
            this.checkBoxSum.AutoSize = true;
            this.checkBoxSum.Location = new System.Drawing.Point(6, 9);
            this.checkBoxSum.Name = "checkBoxSum";
            this.checkBoxSum.Size = new System.Drawing.Size(127, 17);
            this.checkBoxSum.TabIndex = 21;
            this.checkBoxSum.Text = "Show summary graph";
            this.checkBoxSum.UseVisualStyleBackColor = true;
            this.checkBoxSum.CheckedChanged += new System.EventHandler(this.checkBoxSum_CheckedChanged);
            // 
            // comboBoxProxy
            // 
            this.comboBoxProxy.FormattingEnabled = true;
            this.comboBoxProxy.Items.AddRange(new object[] {
            "All proxies"});
            this.comboBoxProxy.Location = new System.Drawing.Point(1324, 467);
            this.comboBoxProxy.Name = "comboBoxProxy";
            this.comboBoxProxy.Size = new System.Drawing.Size(115, 21);
            this.comboBoxProxy.TabIndex = 22;
            this.comboBoxProxy.Visible = false;
            this.comboBoxProxy.SelectedIndexChanged += new System.EventHandler(this.comboBoxProxy_SelectedIndexChanged);
            // 
            // labelProxy
            // 
            this.labelProxy.AutoSize = true;
            this.labelProxy.Location = new System.Drawing.Point(1321, 451);
            this.labelProxy.Name = "labelProxy";
            this.labelProxy.Size = new System.Drawing.Size(112, 13);
            this.labelProxy.TabIndex = 23;
            this.labelProxy.Text = "Show agents of proxy:";
            this.labelProxy.Visible = false;
            // 
            // textBoxSum
            // 
            this.textBoxSum.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBoxSum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSum.Location = new System.Drawing.Point(100, 29);
            this.textBoxSum.Name = "textBoxSum";
            this.textBoxSum.Size = new System.Drawing.Size(19, 13);
            this.textBoxSum.TabIndex = 24;
            this.textBoxSum.Text = "2";
            this.textBoxSum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxSum_KeyDown);
            // 
            // panelTaskSessions
            // 
            this.panelTaskSessions.AutoScroll = true;
            this.panelTaskSessions.Location = new System.Drawing.Point(15, 43);
            this.panelTaskSessions.Name = "panelTaskSessions";
            this.panelTaskSessions.Size = new System.Drawing.Size(263, 504);
            this.panelTaskSessions.TabIndex = 25;
            // 
            // labelDragAndDropMessage
            // 
            this.labelDragAndDropMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDragAndDropMessage.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.labelDragAndDropMessage.Location = new System.Drawing.Point(346, 165);
            this.labelDragAndDropMessage.Name = "labelDragAndDropMessage";
            this.labelDragAndDropMessage.Size = new System.Drawing.Size(954, 193);
            this.labelDragAndDropMessage.TabIndex = 26;
            this.labelDragAndDropMessage.Text = "Drag and drop Task .log files here";
            this.labelDragAndDropMessage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelSum
            // 
            this.labelSum.Location = new System.Drawing.Point(6, 29);
            this.labelSum.Name = "labelSum";
            this.labelSum.Size = new System.Drawing.Size(172, 16);
            this.labelSum.TabIndex = 27;
            this.labelSum.Text = "One sum point per         x10 sec";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxSum);
            this.groupBox3.Controls.Add(this.checkBoxSum);
            this.groupBox3.Controls.Add(this.labelSum);
            this.groupBox3.Location = new System.Drawing.Point(1271, 506);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(201, 47);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1484, 733);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.panelTaskSessions);
            this.Controls.Add(this.labelProxy);
            this.Controls.Add(this.comboBoxProxy);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.groupBoxTaskSessions);
            this.Controls.Add(this.labelY);
            this.Controls.Add(this.checkBoxAutoY);
            this.Controls.Add(this.textBoxMaxY);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.Minus);
            this.Controls.Add(this.Plus);
            this.Controls.Add(this.Right);
            this.Controls.Add(this.Left);
            this.Controls.Add(this.chart1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.labelDragAndDropMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PerfAnalyzer";
            this.Shown += new System.EventHandler(this.MainForm_Shown);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.RadioButton radioButtonRead;
        private System.Windows.Forms.RadioButton radioButtonTransferred;
        private System.Windows.Forms.Button Left;
        private System.Windows.Forms.Button Right;
        private System.Windows.Forms.Button Plus;
        private System.Windows.Forms.Button Minus;
        private System.Windows.Forms.RadioButton radioButtonLine;
        private System.Windows.Forms.RadioButton radioButtonPoint;
        private System.Windows.Forms.RadioButton radioButtonColumn;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonProgress;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxMaxY;
        private System.Windows.Forms.CheckBox checkBoxAutoY;
        private System.Windows.Forms.Label labelY;
        private System.Windows.Forms.RadioButton radioButtonPexSource;
        private System.Windows.Forms.RadioButton radioButtonPexTarget;
        private System.Windows.Forms.RadioButton radioButtonPexNetworkS;
        private System.Windows.Forms.RadioButton radioButtonPexProxy;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBoxTaskSessions;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.RadioButton radioButtonPexNetworkT;
        private System.Windows.Forms.CheckBox checkBoxSum;
        private System.Windows.Forms.ComboBox comboBoxProxy;
        private System.Windows.Forms.Label labelProxy;
        private System.Windows.Forms.TextBox textBoxSum;
        private System.Windows.Forms.Panel panelTaskSessions;
        private System.Windows.Forms.Label labelDragAndDropMessage;
        private System.Windows.Forms.Label labelSum;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}

