﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using PerfAnalyzer.Domain;
//using TaskComboBox = PerfAnalyzer.Domain.TaskComboBox;
using PerfAnalyzer.Presentation;
using PerfAnalyzer.Exceptions;
using System.Windows.Forms.DataVisualization.Charting;

namespace PerfAnalyzer
{
    public partial class MainForm : Form
    {
        List<TaskComboBox> taskComboBoxes = new List<TaskComboBox>();
        public ILogger logger;
        Presenter _presenter;
        private readonly string[] _args;

        public MainForm(Presenter presenter, string[] args)
        {
            InitializeComponent();
            _presenter = presenter;
            _args = args;
        }

        public new void Show()
        {
            Application.Run(this);
        }

        public void GenerateTaskComboBox(string filename, List<string> taskRunSessionsNames)
        {
            var shortFileName = Path.GetFileName(filename);
            var taskComboBox = TaskComboBoxCreateEmptyItem(shortFileName);

            SetComboBoxItemsNames(ref taskComboBox, taskRunSessionsNames);

            taskComboBoxes.Add(taskComboBox);
        }

        TaskComboBox TaskComboBoxCreateEmptyItem(string shortFileName)
        {
            var taskComboBox = new TaskComboBox();
            taskComboBox.labelTask.Text = $"{shortFileName}";
            taskComboBox.comboBoxTask.SelectedIndexChanged += ComboBoxTask_SelectedIndexChanged;
            panelTaskSessions.Controls.Add(taskComboBox.comboBoxTask);
            panelTaskSessions.Controls.Add(taskComboBox.labelTask);

            return taskComboBox;
        }

        void SetComboBoxItemsNames(ref TaskComboBox taskComboBox, List<string> comboBoxItemsNames)
        {
            foreach (string str in comboBoxItemsNames)
            {
                taskComboBox.comboBoxTask.Items.Add(str);
            }

            if (taskComboBox.comboBoxTask.Items.Count > 0)
            {
                taskComboBox.comboBoxTask.SelectedIndex = taskComboBox.comboBoxTask.Items.Count - 1;
            }
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = openFileDialog1.FileName;

            _presenter.HandleSingleFile(filename);
        }

        private void ComboBoxTask_SelectedIndexChanged(object sender, EventArgs e)
        {
            //reset scale
            chart1.ChartAreas[0].AxisX.Minimum = Double.NaN;
            chart1.ChartAreas[0].AxisX.Maximum = Double.NaN;

            checkBoxAutoY.Checked = true;
            textBoxMaxY.Enabled = false;

            if (taskComboBoxes.Count > 0)
            {
                _presenter.RefreshGraph();
            }
        }

        public void AddProxyToComboBox(string proxyname)
        {
            comboBoxProxy.Items.Add(proxyname);
        }

        public void Draw(List<TaskFile> taskFiles)
        {
            GraphType graph = SelectGraphTypeToDraw();
            chart1.Series.Clear();

            try
            {
                FillChartWithData(taskFiles, graph);

                if (checkBoxAutoY.Checked)
                {
                    SetYScaleToAuto();
                }

                HideSeriesForNotSelectedProxies();

                //Summary graph (for Read and Transferred graphs only)
                if (checkBoxSum.Checked
                    && (radioButtonRead.Checked || radioButtonTransferred.Checked)
                    )
                {
                    GenerateSerieForSummaryGraph();
                }

                ShowGraphControls();
            }
            catch
            {
                MessageBox.Show($"Ooops, something went wrong\nSome data can be displayed incorrectly", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void GenerateSerieForSummaryGraph()
        {
            DateTime starttime;
            DateTime endtime;

            (starttime, endtime) = CalculateTimeRange();

            AddSummarySerieToChart();

            InitializeSummarySerieWithEmptyData(starttime, endtime);

            FillSummarySerieWithData();

        }
        void FillSummarySerieWithData()
        {
            foreach (Series serie in chart1.Series)
            {
                //skip adding point of "SUM" serie
                if (serie.Name == "SUM")
                    break;

                //skip disabled series
                if (serie.Enabled == false)
                    continue;

                int j = 0;
                int count = 0;
                double acc = 0;
                for (int p = 0; p < serie.Points.Count; p++)
                {

                    while (j < chart1.Series[chart1.Series.Count - 1].Points.Count - 1)
                    {
                        double min = chart1.Series[chart1.Series.Count - 1].Points[j].XValue;
                        double max = chart1.Series[chart1.Series.Count - 1].Points[j + 1].XValue;
                        if (serie.Points[p].XValue >= min && serie.Points[p].XValue < max)
                        {
                            acc += serie.Points[p].YValues[0];
                            count++;
                            break;
                        }
                        if (count > 0)
                        {
                            chart1.Series[chart1.Series.Count - 1].Points[j + 1].YValues[0] += acc / count;
                            count = 0;
                            acc = 0;
                        }
                        j++;
                    }
                    //adding very last points:
                    if (p == (serie.Points.Count - 1))
                        chart1.Series[chart1.Series.Count - 1].Points[j + 1].YValues[0] += acc / count;
                }
            }
        }

        void InitializeSummarySerieWithEmptyData(DateTime startTime, DateTime endTime)
        {
            //generate empty data                 
            DateTime i = startTime;
            chart1.Series[chart1.Series.Count - 1].Points.AddXY(i, 0);

            int timeIncrement = SetScaleForSumPoints();

            while (i <= endTime)
            {
                i = i.AddSeconds(timeIncrement);    //average of 2 intervals - 20 seconds                    
                chart1.Series[chart1.Series.Count - 1].Points.AddXY(i, 0);
            }
        }

        int SetScaleForSumPoints()
        {
            int timeIncrement = 20;    //default = 20 seconds
            int parseResult;
            if (int.TryParse(textBoxSum.Text, out parseResult))
            {
                if (parseResult < 2)
                {
                    parseResult = 2;
                }
                timeIncrement = parseResult * 10;
            }
            textBoxSum.Text = (timeIncrement/10).ToString();

            return timeIncrement;
        }

        void AddSummarySerieToChart()
        {
            chart1.Series.Add("SUM");
            chart1.Series[chart1.Series.Count - 1].Color = Color.Black;
            chart1.Series[chart1.Series.Count - 1].BorderWidth = 4;
            if (radioButtonPoint.Checked)
                chart1.Series[chart1.Series.Count - 1].ChartType = SeriesChartType.Point;
            else
                chart1.Series[chart1.Series.Count - 1].ChartType = SeriesChartType.Line;

            chart1.Series[chart1.Series.Count - 1].XValueType = ChartValueType.DateTime;
            chart1.Series[chart1.Series.Count - 1].YValueType = ChartValueType.Double;
        }

        (DateTime starttime, DateTime endtime) CalculateTimeRange()
        {
            DateTime starttime = DateTime.Now;
            DateTime endtime = DateTime.MinValue;
            foreach (Series serie in chart1.Series)
            {
                //skip disabled series
                if (serie.Enabled == false)
                    continue;
                if (serie.Points.Count > 0)     //skip agents without data
                {
                    if (serie.Points[0].XValue < starttime.ToOADate())
                        starttime = DateTime.FromOADate(serie.Points[0].XValue);
                    if (serie.Points[serie.Points.Count - 1].XValue > endtime.ToOADate())
                        endtime = DateTime.FromOADate(serie.Points[serie.Points.Count - 1].XValue);
                }
            }
            return (starttime, endtime);
        }

        void HideSeriesForNotSelectedProxies()
        {
            if (comboBoxProxy.SelectedIndex != 0)    //custom proxy selected (not "All proxies")
            {
                for (int i = 0; i < chart1.Series.Count; i++)
                {
                    string str = (comboBoxProxy.Items[comboBoxProxy.SelectedIndex]).ToString();
                    if ((chart1.Series[i].Name).Contains(str) == false)
                    {
                        chart1.Series[i].Enabled = false;
                    }
                }
            }
        }
        void FillChartWithData(List<TaskFile> taskFiles, GraphType graphtype)
        {
            int num = 0;
            foreach (TaskFile task in taskFiles)
            {
                var SelectedSession = taskComboBoxes[num].getSelectedIndex();
                num++;

                if (task.TaskRuns.Count > 0)    //skip empty files without suitable data
                {
                    AddAgentsToGraph(task.TaskRuns[SelectedSession].Agents, graphtype);
                }
            }
        }

        void AddAgentsToGraph(List<Agent> agents, GraphType graphtype)
        {
            foreach (var agent in agents)
            {
                int avrg_transfer_speed = 0;
                if (agent.Pexes.Count > 0)
                {
                    avrg_transfer_speed = (int)((double)(agent.Pexes[agent.Pexes.Count - 1].DataTransferred / 1024 / 1024) / ((agent.Pexes[agent.Pexes.Count - 1].TimeStamp - agent.Pexes[0].TimeStamp).TotalSeconds + 1)); //+1 second - to avoid 0 between timestamps                           
                }
                string SerieName = $"Agent {agent.AgentName}: {agent.DiskName}\navrg. {avrg_transfer_speed} MB/s, proxy: \"{agent.ProxyName}\" ";
                chart1.Series.Add(SerieName);

                chart1.Series[0].Color = Color.Red; //color for very first graph

                SetGraphStyle();

                chart1.ChartAreas[0].AxisX.LabelStyle.Format = "dd.MM.yy  HH:mm:ss";
                chart1.Series[SerieName].Points.DataBindXY(agent.GetX(), agent.GetY(graphtype));
            }
        }

        void SetGraphStyle()
        {
            if (radioButtonLine.Checked)
                for (int i = 0; i < chart1.Series.Count; i++)
                {
                    chart1.Series[i].ChartType = SeriesChartType.Line;
                    chart1.Series[i].BorderWidth = 2;
                }
            else if (radioButtonPoint.Checked)
            {
                for (int i = 0; i < chart1.Series.Count; i++)
                    chart1.Series[i].ChartType = SeriesChartType.Point;
            }
            else if (radioButtonColumn.Checked)
            {
                for (int i = 0; i < chart1.Series.Count; i++)
                    chart1.Series[i].ChartType = SeriesChartType.Area;
            }
        }

        void SetYScaleToAuto()
        {
            chart1.ChartAreas[0].AxisY.Maximum = Double.NaN;
        }
        void ShowGraphControls()
        { 
            if (taskComboBoxes.Count > 0)
            {
                chart1.Visible = true;
                labelY.Visible = true;
                labelY.BackColor = chart1.BackColor;
                textBoxMaxY.Visible = true;
                textBoxMaxY.BackColor = chart1.BackColor;
                checkBoxAutoY.Visible = true;
                checkBoxAutoY.BackColor = chart1.BackColor;
                labelProxy.Visible = true;
                labelProxy.BackColor = chart1.BackColor;
                comboBoxProxy.Visible = true;
            }
        }
        public void HideGraphControls()
        {
            chart1.Visible = false;
            labelY.Visible = false;
            textBoxMaxY.Visible = false;
            checkBoxAutoY.Visible = false;
            labelProxy.Visible = false;
            comboBoxProxy.Visible = false;
        }

        GraphType SelectGraphTypeToDraw()
        {
            GraphType graph;
            if (radioButtonRead.Checked) graph = GraphType.Read;
            else if (radioButtonTransferred.Checked) graph = GraphType.Transferred;
            else if (radioButtonProgress.Checked) graph = GraphType.Progress;
            else if (radioButtonProgress.Checked) graph = GraphType.Progress;
            else if (radioButtonPexSource.Checked) graph = GraphType.PexSource;
            else if (radioButtonPexProxy.Checked) graph = GraphType.PexProxy;
            else if (radioButtonPexNetworkS.Checked) graph = GraphType.PexNetworkS;
            else if (radioButtonPexNetworkT.Checked) graph = GraphType.PexNetworkT;
            else if (radioButtonPexTarget.Checked) graph = GraphType.PexTarget;
            else graph = GraphType.Transferred;

            return graph;
        }

        private void Refresh_Display(object sender, EventArgs e)
        {
            _presenter.RefreshGraph();
        }

        private void Plus_Click(object sender, EventArgs e)
        {
            double min = chart1.ChartAreas[0].AxisX.Minimum;
            double max = chart1.ChartAreas[0].AxisX.Maximum;
            chart1.ChartAreas[0].AxisX.Minimum = min + (max - min) / 4;
            chart1.ChartAreas[0].AxisX.Maximum = max - (max - min) / 4;
        }

        private void Minus_Click(object sender, EventArgs e)
        {
            double min = chart1.ChartAreas[0].AxisX.Minimum;
            double max = chart1.ChartAreas[0].AxisX.Maximum;
            chart1.ChartAreas[0].AxisX.Minimum = min - (max - min) / 4;
            chart1.ChartAreas[0].AxisX.Maximum = max + (max - min) / 4;
        }

        private void Left_Click(object sender, EventArgs e)
        {
            double min = chart1.ChartAreas[0].AxisX.Minimum;
            double max = chart1.ChartAreas[0].AxisX.Maximum;
            chart1.ChartAreas[0].AxisX.Minimum = min - (max - min) / 10;
            chart1.ChartAreas[0].AxisX.Maximum = max - (max - min) / 10;
        }

        private void Right_Click(object sender, EventArgs e)
        {
            double min = chart1.ChartAreas[0].AxisX.Minimum;
            double max = chart1.ChartAreas[0].AxisX.Maximum;
            chart1.ChartAreas[0].AxisX.Minimum = min + (max - min) / 10;
            chart1.ChartAreas[0].AxisX.Maximum = max + (max - min) / 10;
        }

        private void textBoxMaxY_TextChanged(object sender, EventArgs e)
        {
            string s = textBoxMaxY.Text;
            try
            {
                chart1.ChartAreas[0].AxisY.Maximum = (Convert.ToDouble(s));
            }
            catch
            { }
        }

        private void checkBoxAutoY_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxAutoY.Checked)
                textBoxMaxY.Enabled = false;
            else textBoxMaxY.Enabled = true;
            _presenter.RefreshGraph();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormAbout f = new FormAbout();
            f.StartPosition = FormStartPosition.CenterParent;
            f.ShowDialog(this);
        }

        private void chart1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            _presenter.HandleSeveralFiles(files);
        }

        private void chart1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;

        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            _presenter.HandleSeveralFiles(files);
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            _presenter.ClearAll();
        }

        public void ClearTaskComboBoxes()
        {
            for (int i = 0; i < TaskComboBox.TaskCounter; i++)
            {
                taskComboBoxes[i].comboBoxTask.Dispose();
                taskComboBoxes[i].labelTask.Dispose();
            }
            taskComboBoxes.RemoveRange(0, taskComboBoxes.Count);
            TaskComboBox.TaskCounter = 0;
            TaskComboBox.top = TaskComboBox.TOP;

            labelY.Visible = false;
            textBoxMaxY.Visible = false;
            checkBoxAutoY.Visible = false;
            chart1.Visible = false;

            comboBoxProxy.Items.Clear();
            comboBoxProxy.Items.Add("All proxies");
            comboBoxProxy.SelectedIndex = 0;
        }

        private void comboBoxProxy_SelectedIndexChanged(object sender, EventArgs e)
        {
            _presenter.RefreshGraph();
        }

        private void checkBoxSum_CheckedChanged(object sender, EventArgs e)
        {
            _presenter.RefreshGraph();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            comboBoxProxy.SelectedIndex = 0; //by default agents of all proxies are displayed
            HideGraphControls();    //hide controls for graph

            if (_args.Length > 0)   //if file pathes were provided via command line
            {
                foreach (string arg in _args)
                {
                    if (File.Exists(arg))
                    {

                        _presenter.HandleSingleFile(arg);
                    }
                }
            }
        }

        private void textBoxSum_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _presenter.RefreshGraph();
            }
        }
    }
}
