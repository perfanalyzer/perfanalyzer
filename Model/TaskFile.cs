﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfAnalyzer.Domain
{
    public class TaskFile
    {
        public List<TaskRun> TaskRuns { get; set; }
        //public int SelectedSession { get; set; }
        public TaskFile()
        {
            TaskRuns = new List<TaskRun>();
        }
        public void ClearTaskRuns()
        {
            //SelectedSession = 0;
            TaskRuns.Clear();
        }
    }
}
