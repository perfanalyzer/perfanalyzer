﻿using PerfAnalyzer.Presentation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfAnalyzer.Domain
{
    public class Agent
    {
        public string AgentName { get; set; }
        public string DiskName { get; set; }
        public string ProxyName { get; set; }

        public List<DateTime> X { get; set; }
        public List<int> Y { get; set; }
        public List<Pex> Pexes { get; set; }

        public Agent (string agentName, string diskName, string proxyName)
        {
            AgentName = agentName;
            DiskName = diskName;
            ProxyName = proxyName;
            X = new List<DateTime>();
            Y = new List<int>();
            Pexes = new List<Pex>();
        }

        public DateTime[] GetX()
        {
            return Pexes.Select(p => p.TimeStamp).ToArray();
        }
        public double[] GetY(GraphType graph)
        {
            if (graph == GraphType.Read)
                return Pexes.Select(p => p.DataReadSpeed).ToArray();
            if (graph == GraphType.Transferred)
                return Pexes.Select(p => p.DataTransferredSpeed).ToArray();
            if (graph == GraphType.Progress)
                return Pexes.Select(p => (double)p.Progress).ToArray();
            if (graph == GraphType.PexSource)
                return Pexes.Select(p => (double)p.Pex1).ToArray();
            if (graph == GraphType.PexProxy)
                return Pexes.Select(p => (double)p.Pex2).ToArray();
            if (graph == GraphType.PexNetworkS)
                return Pexes.Select(p => (double)p.Pex3).ToArray();
            if (graph == GraphType.PexNetworkT)
                return Pexes.Select(p => (double)p.Pex4).ToArray();
            if (graph == GraphType.PexTarget)
                return Pexes.Select(p => (double)p.Pex6).ToArray();
            else
                return Pexes.Select(p => p.DataTransferredSpeed).ToArray();
        }
    }
}
