﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfAnalyzer.Domain
{
    public class TaskRun
    {
        public List<Agent> Agents = new List<Agent>();
        public List<string> AgentStarts = new List<string>();
        public DateTime TaskRunStartTime;
        public DateTime TaskRunEndTime;

        public TaskRun()
        {
        }
    }
}
