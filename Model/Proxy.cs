﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfAnalyzer.Model
{
    public class Proxy
    {
        public string Name { get; set; }
        public Proxy(string name)
        {
            Name = name;
        }
    }
}
