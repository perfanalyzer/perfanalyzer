﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PerfAnalyzer.Domain
{
    public struct Pex
    {
        public DateTime TimeStamp;   //28.12.2020 15:16:09
        public string AgentName;   //f367
        public int Progress;       //54
        public long DataRead;       //232188280832
        public long DataTransferred; //232190716840
        public int Pex1;
        public int Pex2;
        public int Pex3;
        public int Pex4;
        public int Pex5;
        public int Pex6;

        public double DataReadSpeed;
        public double DataTransferredSpeed;

        public double CalculateDataReadSpeed(Pex currentPex, Pex previousPex)
        {

            double time_seconds = (currentPex.TimeStamp - previousPex.TimeStamp).TotalSeconds;

            if (time_seconds == 0)      //to avoid divide by zero
                time_seconds = 1;

            currentPex.DataReadSpeed = (currentPex.DataRead - previousPex.DataRead) / time_seconds / 1048576; // MB/s

            //due to the fact that the same agent can be used for processing of several facts, previous DataRead can be higher than current DataRead
            //set it to 0 as a workaround
            if (currentPex.DataReadSpeed < 0)  
            {
                currentPex.DataReadSpeed = 0;
            }

            return currentPex.DataReadSpeed;
        }
        public double CalculateDataTransferredSpeed(Pex currentPex, Pex previousPex)
        {

            double time_seconds = (currentPex.TimeStamp - previousPex.TimeStamp).TotalSeconds;

            if (time_seconds == 0)      //to avoid divide by zero
                time_seconds = 1;

            currentPex.DataTransferredSpeed = (currentPex.DataTransferred - previousPex.DataTransferred) / time_seconds / 1048576.0;   // MB/s

            if (currentPex.DataTransferredSpeed < 0)
            {
                currentPex.DataTransferredSpeed = 0;
            }

            return currentPex.DataTransferredSpeed;
        }
    }
}
